// import { Component } from '@angular/core';
import { PrintService } from './print.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AgGridAngular } from 'ag-grid-angular';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @ViewChild('agGrid', { static: true }) agGrid: AgGridAngular;

  rowData: any;
  title = 'report-gen';

  liveArray: any[] = [];

  constructor(public printService: PrintService, private http: HttpClient) { }

  columnDefs = [
    {
      width: 100,
      headerCheckboxSelection: true,
      resizable: true,
      checkboxSelection: true,
    },
    { headerName: 'Make', field: 'make', sortable: true, filter: true },
    { headerName: 'Model', field: 'model', sortable: true, filter: true },
    { headerName: 'Price', field: 'price', sortable: true, filter: true }
  ];


  ngOnInit() {
    this.rowData = this.http.get('https://api.myjson.com/bins/15psn9');
  }

  getSelectedRows() {
    const selectedNodes = this.agGrid.api.getSelectedNodes();
    const selectedData = selectedNodes.map(node => node.data);
    debugger
    const selectedDataStringPresentation = selectedData.map(node => node.make + ' ' + node.model).join(', ');
    alert(`Selected nodes: ${selectedDataStringPresentation}`);
  }

  onPrintInvoice() {
    const invoiceIds = ['101', '102'];
    this.printService
      .printDocument('invoice');
  }

  onGridReady(event: any) {
    let isSelected = event.node.isSelected();
    if (isSelected) {
      this.liveArray.push(event.data)
    } else {
      let index: number = this.liveArray.findIndex(x => x.model == event.data.model);
      this.liveArray.splice(index, 1);
    }
  }

  genarateReport(){
    console.log('====================================');
    console.log(this.liveArray);
    console.log('====================================');
  }
}

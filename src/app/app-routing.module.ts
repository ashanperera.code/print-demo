import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportViewComponent } from './report-view/report-view.component';
import { AppComponent } from './app.component';
import { PrintLayoutComponent } from './print-layout/print-layout.component';


const routes: Routes = [
  // { path: '', redirectTo: 'app', pathMatch: 'full' },
  // { path: 'app', component: AppComponent },
   { path: 'report-view', component: ReportViewComponent },
  // {
  //   path: 'print',
  //   outlet: 'print',
  //   component: PrintLayoutComponent,
  //   children: [
  //     { path: 'invoice', component: ReportViewComponent }
  //   ]
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

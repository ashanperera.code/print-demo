import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReportViewComponent } from './report-view/report-view.component';
import { ReportPrintComponent } from './report-print/report-print.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { PrintLayoutComponent } from './print-layout/print-layout.component';
import { PrintService } from './print.service';
import {NgxPrintModule} from 'ngx-print';
import { AgGridModule } from 'ag-grid-angular';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    ReportViewComponent,
    ReportPrintComponent,
    PrintLayoutComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    NgxPrintModule,
    AgGridModule.withComponents([]),
    HttpClientModule
  ],
  providers: [PrintService],
  bootstrap: [AppComponent]
})
export class AppModule { }
